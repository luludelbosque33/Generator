const Generator = require('../../abstractGenerator');
const {randRange, randomElement, bigModulo} = require('../../helpers');
const handleParams = require('../../handleParams');

module.exports = class INSEE extends Generator {
    params() {
        return {
            birthyear: {
                type: 'int',
                min: 1800,
                max: 2200,
            },
            birthmonth: {
                type: 'int',
                min: 1,
                max: 12,
            },
            birthmonthunknown: {
                type: 'bool',
            },
            birthplace: {
                type: 'int',
                pattern: '^\d{5}$',
            },
            birtplaceunknown: {
                type: 'bool',
            },
            birtplacedomestic: {
                type: 'bool',
            },
            birtplaceabroad: {
                type: 'bool',
            },
            gender: {
                type: 'string',
                values: ['m', 'f', 'mf'],
                default: 'mf',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const genderNumbers = [];
        if (params.gender.includes('m')) {
            genderNumbers.push('1');
        }
        if (params.gender.includes('f')) {
            genderNumbers.push('2');
        }
        const gender = randomElement(genderNumbers);

        const year = (params.birthyear ? (params.birthyear % 100) : randRange(0, 100)).toString().padStart(2, '0');
        const month = (params.birthmonthunknown
            ? randRange(21, 100)
            : (params.birthmonth || randRange(1, 13))
        ).toString().padStart(2, '0');

        let placeOptions = [];
        if (params.birthplace) {
            placeOptions.push(params.birthplace);
        }
        if (!params.birthplace && params.birtplaceunknown) {
            placeOptions.push(randRange(0, 100).toString() + '990');
        }
        if (!params.birthplace && params.birtplacedomestic) {
            placeOptions.push(randRange(0, 99).toString() + randRange(101, 990).toString());
        }
        if (!params.birthplace && params.birtplaceabroad) {
            placeOptions.push('99' + randRange(101, 990).toString());
        }
        const place = randomElement(placeOptions).padStart(5, '0');

        const serial = randRange(1, 1000).toString().padStart(3, '0');

        const nr = parseInt(gender + year + month + place + serial);

        const checksum = bigModulo(nr, 97);

        return nr.toString() + (checksum || 97).toString().padStart(2, '0');
    }

    validate(value) {
        const match = value.match(/^(1|2)(\d{2})(\d{2})(\d{5})(\d{3})(\d{2})$/);
        if (!match) {
            throw new Error('format')
        }

        const [, gender, year, month, place, serial, checksum] = match;

        const m = parseInt(month);

        if (!m || (m > 12) && (m < 20)) {
            throw new Error('birthdate')
        }

        const validChecksum = bigModulo(parseInt(value.substr(0, 13)), 97) || 97;

        if (!checksum || validChecksum !== parseInt(checksum)) {
                throw new Error('checksum')
        }

        return {
            gender: gender === '1' ? 'm' : 'f',
            birthdate: `'${year}-${m <= 12 ? month : '(uknown)'}`,
            birthplace: `${place} (${this._getPlaceType(place)})`,
        };
    }

    _getPlaceType(code) {
        if (!parseInt(code.substr(0, 2)) || !parseInt(code.substr(2, 3))) {
            throw new Error('placeCode');
        }
        if (code.substr(2, 3) === '990') {
            return 'unknown';
        }
        if (code.substr(0, 2) === '99') {
            return 'abroad';
        }

        return 'domestic';
    }
};
