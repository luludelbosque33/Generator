const Generator = require('../../abstractGenerator');
const {randomElement, randomise, letterToNumber, bigModulo} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class IBAN extends Generator {
    static get countries() {
        return {
            'AD': 24, 'AE': 23, 'AL': 28, 'AT': 20, 'AZ': 28, 'BA': 20, 'BE': 16, 'BG': 22, 'BH': 22, 'BR': 29,
            'CH': 21, 'CR': 21, 'CY': 28, 'CZ': 24, 'DE': 22, 'DK': 18, 'DO': 28, 'EE': 20, 'ES': 24, 'FI': 18,
            'FR': 27, 'FO': 18, 'GB': 22, 'GE': 22, 'GI': 23, 'GL': 18, 'GR': 27, 'GT': 28, 'HR': 21, 'HU': 28,
            'IE': 22, 'IL': 23, 'IS': 26, 'IT': 27, 'JO': 30, 'KW': 30, 'KZ': 20, 'LB': 28, 'LI': 21, 'LT': 20,
            'LU': 20, 'LV': 21, 'MC': 27, 'MD': 24, 'ME': 22, 'MK': 19, 'MR': 27, 'MT': 31, 'MU': 30, 'NL': 18,
            'NO': 15, 'PK': 24, 'PL': 28, 'PT': 25, 'QA': 29, 'RO': 24, 'RS': 22, 'SA': 24, 'SE': 24, 'SI': 19,
            'SK': 24, 'SM': 27, 'TN': 24, 'TR': 26, 'VG': 24,
        }
    }

    params() {
        return {
            country: {
                type: 'string',
                values: Object.keys(IBAN.countries),
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const countryCode = params.country || randomElement(Object.keys(IBAN.countries));

        const nr = randomise(digits, IBAN.countries[countryCode] - 4);
        const nrBig = nr + letterToNumber(countryCode[0]) + letterToNumber(countryCode[1]) + '00';
        const modulo = bigModulo(nrBig, 97);
        const checksum = ((97 - modulo) + 1) % 97;
        const iban = countryCode + checksum.toString().padStart(2, '0') + nr;

        return iban.replace(/\S{4}/g, m => m + ' ').trim();
    }

    validate(value) {
        value = value.replace(/\s/g, '');

        if (!value.match(/^\D{2}\d+$/g)) {
            throw new Error('format')
        }

        const countryCode = value.slice(0, 2).toUpperCase();

        if (!(countryCode in IBAN.countries)) {
            throw new Error('countryCode')
        }

        if (value.length !== IBAN.countries[countryCode]) {
            throw new Error('length')
        }

        value = value.substr(4) + letterToNumber(countryCode[0]) + letterToNumber(countryCode[1]) + value.substr(2, 2);

        if (bigModulo(value, 97) !== 1) {
            throw new Error('checksum')
        }

        return {
            country: countryCode,
        };
    }
};
