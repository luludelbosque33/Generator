const Generator = require('../../abstractGenerator');
const {randomise, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');

module.exports = class NIP extends Generator {
    static get weights() {
        return [6, 5, 7, 2, 3, 4, 5, 6, 7];
    }

    generate(params = {}) {
        let nr;
        let checksum = 10;
        while ((checksum % 11) === 10) {
            nr = randomise(digits, 9);
            checksum = sum(arrayTimesArray(NIP.weights, nr));
        }

        return nr + (checksum % 11);
    }

    validate(value) {
        value = value.replace(/[- ]/g,'');
        if (!value.match(/^\d{10}$/g)) {
            throw new Error('format')
        }

        const sk = sum(arrayTimesArray(NIP.weights, value.slice(0, 9))) % 11;

        if ((value === 10) || (sk !== parseInt(value[9]))) {
            throw new Error('checksum')
        }

        return true;
    }
};
