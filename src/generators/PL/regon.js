const Generator = require('../../abstractGenerator');
const {randomise, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class REGON extends Generator {
    static get weights() {
        return {
            9: [8, 9, 2, 3, 4, 5, 6, 7],
            14: [2, 4, 8, 5, 0, 9, 7, 3, 6, 1, 2, 4, 8], 
        };
    }

    params() {
        return {
            length: {
                type: 'int',
                values: [9, 14],
                default: 9,
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        let nr = randomise(digits, 8);
        nr += sum(arrayTimesArray(REGON.weights[9], nr)) % 11 % 10;

        if (parseInt(params.length) === 14) {
            nr += randomise(digits, 4);
            nr += sum(arrayTimesArray(REGON.weights[14], nr)) % 11 % 10;
        }
        
        return nr;
    }

    validate(value) {
        if (!value.match(/^(\d{9}|\d{14})$/g)) {
            throw new Error('format');
        }

        if (sum(arrayTimesArray(REGON.weights[9], value.slice(0, 8))) % 11 % 10 !== parseInt(value[8])) {
            throw new Error('checksum')
        }

        if (value.length === 14) {
            if (sum(arrayTimesArray(REGON.weights[14], value.slice(0, 13))) % 11 % 10 !== parseInt(value[13])) {
                throw new Error('checksum')
            }
        }

        return true;
    }
};
