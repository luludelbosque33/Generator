const Generator = require('../../abstractGenerator');
const {randomElement, randomise} = require('../../helpers');
const handleParams = require('../../handleParams');
const {digits} = require('../../sets');

module.exports = class NIF extends Generator {
    static get types() {
        return {
            dni: /^(\d)(\d{7})([A-Z])$/,
            nie: /^([XYZ])(\d{7,8})([A-Z])$/,
            nif: /^([KLM])(\d{7})([A-Z])$/,
            cif: /^([ABCDEFGHJNPQRSUVW])(\d{7})([0-9A-J])$/,
        }
    }

    static get dniReplacements() {
        return { 'X': 0, 'Y': 1, 'Z': 2, }
    }

    static get dniChecksumLetters() {
        return 'TRWAGMYFPDXBNJZSQVHLCKE';
    }

    static get nifChecksumLetters() {
        return 'JABCDEFGHI';
    }

    static get nifPrefixesRquiringLetterChecksum() {
        return 'KLMNPQRSW';
    }

    params() {
        return {
            type: {
                type: 'string',
                values: [''].concat(Object.keys(NIF.types)),
                default: '',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const type = params.type || randomElement(Object.keys(NIF.types));

        const prefix = randomise(this._getPrefixChoices(type), 1);

        const nr = randomise(digits, 7);

        const checksum = this._calcChecksum(type, prefix, nr);

        return prefix + nr + checksum;
    }

    _getPrefixChoices(type) {
        switch (type) {
            case 'dni':
                return digits;
            case 'nie':
                return 'XYZ';
            case 'nif':
                return 'KLM';
            case 'cif':
                return 'ABCDEFGHJNPQRSUVW';
        }
    }

    _calcChecksum(type, prefix, nr) {
        if (type === 'nie') {
            prefix = NIF.dniReplacements[prefix];
            type = 'dni';
        }

        if (type === 'dni') {
            return NIF.dniChecksumLetters.charAt(parseInt(prefix + nr) % 23);
        }

        let sum = 0;
        for (let i = 0; i < nr.length; i++) {
            let n = parseInt(nr[i]);
            if (i % 2 === 0) {
                n *= 2;
                n = n < 10 ? n : n - 9;
            }
            sum += n;
        }

        const cDigit = (10 - sum.toString().substr(-1)).toString().substr(-1);
        const cLetter = NIF.nifChecksumLetters.substr(cDigit, 1);

        return NIF.nifPrefixesRquiringLetterChecksum.indexOf(prefix) > -1 ? cLetter : cDigit;
    }

    validate(value) {
        const [type, prefix, nr, checksum] = this._matchType(value);

        const validChecksum = this._calcChecksum(type, prefix, nr);

        if (checksum !== validChecksum) {
            throw new Error('checksum');
        }

        return { type };
    }

    _matchType(value) {
        for (let type in NIF.types) {
            if (NIF.types.hasOwnProperty(type)) {
                const matches = value.match(NIF.types[type]);
                if (matches) {
                    const [, prefix, nr, checksum] = matches;
                    return [type, prefix, nr, checksum];
                }
            }
        }

        throw new Error('format');
    }
};
