const Generator = require('../../abstractGenerator');
const {randomise, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class UTR extends Generator {
    static get weights() {
        return [6, 7, 8, 9, 10, 5, 4, 3, 2];
    }

    generate(params = {}) {
        let nr = randomise(digits, 9);

        const checksum = 11 - sum(arrayTimesArray(UTR.weights, nr)) % 11;

        if (checksum > 9) {
            return this.generate(params);
        }

        return checksum + nr;
    }

    validate(value) {
        value = value.replace(/[- \.]/g,'');
        if (!value.match(/^\d{10}$/g)) {
            throw new Error('format')
        }

        if (11 - sum(arrayTimesArray(UTR.weights, value.slice(1))) % 11 !== parseInt(value[0])) {
            throw new Error('checksum')
        }

        return true;
    }
};
