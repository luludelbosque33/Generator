const Generator = require('../../abstractGenerator');
const {randomElement, randRange} = require('../../helpers');

module.exports = class ITIN extends Generator {
    static get ranges() {
        return [
            [50, 65],
            [70, 88],
            [90, 92],
            [94, 99],
        ]
    }

    generate(params = {}) {
        const g1 = randRange(0, 100);
        const g2range = randomElement(ITIN.ranges);
        const g2 = randRange(g2range[0], g2range[1] + 1);
        const g3 = randRange(1, 10000);

        return `9${g1.toString().padStart(2, '0')}-${g2.toString().padStart(2, '0')}-${g3.toString().padStart(4, '0')}`
    }

    validate(value) {
        const match = value.match(/^(9\d{2})-(\d{2})-(\d{4})$/);
        if (!match) {
            throw new Error('format');
        }

        const g1 = parseInt(match[1]);
        const g2 = parseInt(match[2]);
        const g3 = parseInt(match[3]);
        if (!this._isRangeValid(g2)) {
            throw new Error('format');
        }

        return true;
    }

    _isRangeValid(g2) {
        for (let range of ITIN.ranges) {
            if (g2 >= range[0] && g2 <= range[1]) {
                return true;
            }
        }
        return false;
    }
};
