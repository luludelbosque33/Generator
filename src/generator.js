const GeneratorManager = require('./generatorManager');
const generators = require('./generators');

module.exports = new GeneratorManager(generators);
